<!doctype html>
<?php
//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header('Access-Control-Allow-Headers: Authorization,Origin, Content-Type, X-Auth-Token');
//Header('Access-Control-Allow-Headers', 'api-key,content-type');
//header("Access-Control-Allow-Headers: X-Requested-With");
 


?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap-4.1.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin"  action="index.php" method="post" id="myForm" >
      <h1 class="h3 mb-3 font-weight-normal">Please log in</h1>
      <label for="inputPassword" class="sr-only">Pin</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Pin" name="pin" required>
      <label for="accountId" class="sr-only">Account Id</label>
      <input type="text" id="accountId" class="form-control" placeholder="Account Id"  name="accountId" required>
      <input class="btn btn-lg btn-primary btn-block" type="button" id='sub' value="Log in"></input>
      <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
    </form>
<div id="output">


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.serializeJSON/2.9.0/jquery.serializejson.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
      <script type="text/javascript">
var url = "localhost:8080/api/login";
      var loginBody = {
        "pin": "321"
      }
      
    
      $(document).ready(function(){
        $('#sub').click(function(e) {
          var formData = $("#myForm").serializeJSON();
            $.ajax({
            type: "POST",
            url:url ,
            data: formData,
            crossDomain: true,
            dataType: "json",
            contentType : "application/json",
            success: function (data) {
              $('#output').append(data)              
                
              },
           
          });

           });

  });




    </script>
  </body>
</html>
